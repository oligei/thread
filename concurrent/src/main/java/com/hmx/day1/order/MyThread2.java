package com.hmx.day1.order;

public class MyThread2 extends Thread{
        private Thread thread;
        public MyThread2(String name, Thread thread){
            super(name);
            this.thread=thread;
        }

        @Override
        public void run() {
            System.out.println("I am " +Thread.currentThread().getName());
            try {
                if(thread!=null)
                    thread.join();
                //sleep(200);//要根据线程数量控制时间  保证在该线程执行完毕之前，能够调用join
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println(Thread.currentThread().getName() +" game over");
        }
    }