package com.hmx.day1.interrupt;


public class EndThread4 extends Thread{

    public EndThread4(String name){
        super(name);
    }
    @Override
    public void run() {
        String threadName = Thread.currentThread().getName();
        System.out.println(threadName+" interrupt flag ==========" + isInterrupted());
       while (!isInterrupted()){
           try {
               sleep(5);
           } catch (InterruptedException e) {
               System.out.println(threadName+" interrupt flag =" + isInterrupted());
               interrupt();//阻塞方法要手动中断 sleep,wait,join
               e.printStackTrace();
           }
           System.out.println(threadName+"is running");
           System.out.println(threadName+" interrupt flag =" + isInterrupted());
       }
        System.out.println(threadName+" interrupt flag ==========" + isInterrupted());
    }

    public static void main(String args[]) throws InterruptedException {
            EndThread4 thread = new EndThread4("endThread");
            thread.start();
            sleep(20);
            thread.interrupt();
    }
}
