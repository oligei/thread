package com.hmx.day1.interrupt;

public class EndThread3 extends Thread{

    public EndThread3(String name){
        super(name);
    }
    @Override
    public void run() {
        String threadName = Thread.currentThread().getName();
        System.out.println(threadName+" interrupt flag ==========" + isInterrupted());
       while (!Thread.interrupted()){
           System.out.println(threadName+"is running");
           System.out.println(threadName+" interrupt flag =" + isInterrupted());
       }
        System.out.println(threadName+" interrupt flag ==========" + isInterrupted());
    }

    public static void main(String args[]) throws InterruptedException {
            EndThread3 thread = new EndThread3("endThread");
            thread.start();
            sleep(10);
            thread.interrupt();
    }
}
