package com.hmx.day1.interrupt;


public class EndThread2 extends Thread{

    public EndThread2(String name){
        super(name);
    }
    @Override
    public void run() {
        String threadName = Thread.currentThread().getName();
        System.out.println(threadName+" interrupt flag ==========" + isInterrupted());
       while (!isInterrupted()){
           System.out.println(threadName+"is running");
           System.out.println(threadName+" interrupt flag =" + isInterrupted());
       }
        System.out.println(threadName+" interrupt flag ==========" + isInterrupted());
    }

    public static void main(String args[]) throws InterruptedException {
            EndThread2 thread = new EndThread2("endThread");
            thread.start();
            sleep(20);
            thread.interrupt();
    }
}
